package sda.workshops.Rest.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import sda.workshops.Rest.exception.UserNotFoundException;
import sda.workshops.Rest.model.User;
import sda.workshops.Rest.model.UserList;
import sda.workshops.Rest.service.UserService;

import javax.validation.Valid;
import java.util.List;


@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/users/{name}")
    public User getUserByName(@PathVariable("name") String name) throws UserNotFoundException {
        return userService.getUserByName(name);
    }

    @GetMapping(value = "/users")
    public UserList getUsers() {
        return new UserList(userService.getAllUsers());
    }

    @PostMapping(value = "/users")
    public void addUser(@RequestBody @Valid User user) {
        userService.addUser(user);
    }

    @DeleteMapping(value = "/users/{id}")
    public void deleteUser(@PathVariable Long id) throws UserNotFoundException {
        userService.deleteUser(id);
    }

}
