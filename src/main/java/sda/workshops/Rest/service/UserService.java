package sda.workshops.Rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.workshops.Rest.exception.UserNotFoundException;
import sda.workshops.Rest.model.User;
import sda.workshops.Rest.reposytory.UserRepository;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void deleteUser(Long id) throws UserNotFoundException {

        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("Brak takiego usera"));

        userRepository.delete(user);
    }


    public User getUserByName(String name) throws UserNotFoundException {
        return userRepository.getUserByName(name).orElseThrow(() -> new UserNotFoundException("Brak takiego usera"));
    }

    public List getAllUsers() {
        return userRepository.findAll();
    }

    public void addUser(User user) {
        userRepository.save(user);
    }
}
