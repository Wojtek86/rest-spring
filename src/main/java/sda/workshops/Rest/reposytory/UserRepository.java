package sda.workshops.Rest.reposytory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import sda.workshops.Rest.model.User;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long > {

    Optional<User> getUserByName (@Param("name")String name);

}
